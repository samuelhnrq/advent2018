use std::collections::HashMap;

#[aoc(day2, part1)]
pub fn part_1(input: &str) -> i32 {
    let mut lines = input.split_whitespace().peekable();
    let mut repetitions: Vec<i32> = vec![0; lines.peek().unwrap().len()];
    let mut letters = HashMap::<char, i32>::new();
    for line in lines {
        for letter in line.chars() {
            let curr_value = *letters.get(&letter).unwrap_or(&0);
            letters.insert(letter, curr_value + 1);
        }
        let mut reps = letters.values().filter(|x| **x > 1).collect::<Vec<&i32>>();
        reps.sort_unstable();
        reps.dedup();
        for v in reps {
            repetitions[*v as usize - 2] += 1;
        }
        letters.clear();
    }
    repetitions
        .iter()
        .filter(|z| **z > 0)
        .fold(1, |next, ttl| ttl * next)
}

#[aoc(day2, part2)]
pub fn part_2(input: &str) -> String {
    let lines: Vec<&str> = input.split_whitespace().collect();
    for (i, x) in lines.iter().enumerate() {
        for y in lines.iter().skip(i) {
            let mut diff = 0;
            let mut index = 0;
            for (j, (a, b)) in x.chars().zip(y.chars()).enumerate() {
                if a != b {
                    diff += 1;
                    index = j;
                    if diff > 1 {
                        break;
                    }
                }
            }
            if diff == 1 {
                return format!(
                    "found the result {} and {}, differ at {}# char",
                    x,
                    y,
                    index + 1
                );
            }
        }
    }
    unreachable!();
}
