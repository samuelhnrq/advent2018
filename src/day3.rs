

#[derive(Debug)]
pub struct Claim {
    id: i16,
    x: i32,
    y: i32,
    width: i32,
    height: i32,
}

#[aoc_generator(day3)]
pub fn day3_input_gen(input: &str) -> Vec<Claim> {
    input
        .trim()
        .split('\n')
        .enumerate()
        .map(|(i, line)| {
            let comma_i = line.find(',').unwrap();
            let colon_i = line.find(':').unwrap();
            let at_i = line.find('@').unwrap();
            let x_i = line.find('x').unwrap();
            Claim {
                id: (i + 1) as i16,
                height: line.get(x_i + 1..).unwrap().parse().unwrap(),
                width: line.get(colon_i + 2..x_i).unwrap().parse().unwrap(),
                x: line.get(at_i + 2..comma_i).unwrap().parse().unwrap(),
                y: line.get(comma_i + 1..colon_i).unwrap().parse().unwrap(),
            }
        }).collect()
}

#[aoc(day3, part1)]
pub fn day3(input: &Vec<Claim>) -> i32 {
    let mut fabric: [[i8; 1000]; 1000] = [[0; 1000]; 1000];
    for claim in input {
        for x in claim.x..claim.x + claim.width {
            for y in claim.y..claim.y + claim.height {
                fabric[y as usize][x as usize] += 1;
            }
        }
    }
    fabric
        .iter()
        .map(|x| {
            x.iter()
                .fold(0, |ttl, next| ttl + if *next >= 2 { 1 } else { 0 })
        })
        .sum()
}

#[aoc(day3, part2)]
pub fn day3_part2(input: &Vec<Claim>) -> i16 {
    let mut fabric: [[i16; 1000]; 1000] = [[0; 1000]; 1000];
    let mut contestants: Vec<Option<&Claim>> = input.iter().map(|x| Some(x)).collect();

    for claim in input {
        for x in claim.x..claim.x + claim.width {
            for y in claim.y..claim.y + claim.height {
                let curr = fabric[x as usize][y as usize] as usize;
                if curr != 0 {
                    contestants[claim.id as usize - 1] = None;
                    contestants[curr - 1] = None;
                } else {
                    fabric[x as usize][y as usize] = claim.id;
                }
            }
        }
    }
    contestants.iter().find(|x| x.is_some()).unwrap().unwrap().id
}
